({
    fetchData : function(component, fileId, zipFile,helper) {
        var action = component.get("c.getFile");
        action.setParams({
            'fileId': fileId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var  record = response.getReturnValue();
            if (state === "SUCCESS") {
                console.group('==get file===');
                console.log(record);
                
                zipFile.file(record.fileName + '.' + record.fileExtension, helper.base64ToBlob(record.content));
                zipFile.generateAsync({type:"blob"}).then(function(d){
                    
                    // console.log('====d=====');
                    
                    // console.log(JSON.stringify(d));
                    saveAs(d, "my.zip");
                });
                console.groupEnd();
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action); 
    },
    
    base64ToBlob: function(base64) {
        console.log('===base64toblob====');
        const binaryString = window.atob(base64);
        const len = binaryString.length;
        const bytes = new Uint8Array(len);
        for (let i = 0; i < len; ++i) {
            bytes[i] = binaryString.charCodeAt(i);
        }
        
        return new Blob([bytes]);
    },
    
    fetchFileUsingPromise: function(component, fileId){
        var promiseInstance = new Promise( $A.getCallback( function( resolve , reject ) { 
            var action = component.get("c.getFile");
            action.setParams({
                'fileId': fileId
            });
           
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    resolve(response.getReturnValue());
                }else if(state === "ERROR"){
                    var errors = response.getError();
                    console.error(errors);
                    reject(response.getError() );
                }
            });
            $A.enqueueAction(action);
        }));            
        return promiseInstance;
        
        
    }
    
    
})