({
    drawChart: function (component, helper) { //Define which chart have to be load 
        var chartType = component.get('v.type');
        var chartToDraw = chartType + 'Chart';
        if (chartType === 'column' || chartType === 'bar') chartToDraw = 'columnBarChart';
        if (chartType === 'pie' || chartType === 'donut') chartToDraw = 'pieDonutChart';
        if (chartType === 'stackedcolumn' || chartType === 'stackedbar') chartToDraw = 'stackedColumnBarChart';
        var listOfChartTypes = component.get("v.type").split(',');
        if(listOfChartTypes.length > 1){
            chartToDraw = 'lineAreaHybridChart';
        }
        helper[chartToDraw](component, helper);
        component.set('v.chartRendered', true);
    },
    lineChart: function (component, helper) { // Define Line chart
        var text = "[[x]]:[[value]]";
        var bullettext = "";
        var bulletballoonttext = "";
        var bulletlabeltext = component.get('v.bullettext');
        var bulletballoonlabeltext = component.get('v.bulletballoonttext');
        if(bulletlabeltext == "x"){
            bullettext = "[[x]]";
        }
        else if(bulletlabeltext == "y"){
            bullettext = "[[value]]";
        }
            else if(bulletlabeltext == "x:y"){
                bullettext = "[[x]]:[[value]]";
            }
                else if(bulletlabeltext == "x,y"){
                    bullettext = "[[x]],[[value]]";
                }
                    else{
                        bullettext = "";  
                    }
        
        if(bulletballoonlabeltext == "x"){
            bulletballoonttext = "[[x]]";
        }
        else if(bulletballoonlabeltext == "y"){
            bulletballoonttext = "[[value]]";
        }
            else if(bulletballoonlabeltext == "x:y"){
                bulletballoonttext = "[[x]]:[[value]]";
            }
                else if(bulletballoonlabeltext == "x,y"){
                    bulletballoonttext = "[[x]],[[value]]";
                }
                    else{
                        bulletballoonttext = "";  
                    }
        
        AmCharts.makeChart(component.find('chartContainer').getElement(),
                           {
                               "type": "serial",
                               "theme": "light",
                               "categoryField": "x",
                               "graphs": [
                                   {
                                       "balloonText": bulletballoonttext,
                                       "labelText": bullettext,
                                       "labelPosition": component.get('v.bullettextposition'),
                                       "bullet": component.get('v.bullettype'),
                                       "lineColor": component.get('v.linecolor'),
                                       "valueField": "y"
                                   }
                               ],
                               "titles": [
                                   {
                                       "text": component.get('v.charttitle')
                                   }
                               ],
                               "categoryAxis": {
                                   "gridColor": ""
                               },
                               "valueAxes": [
                                   {
                                       "gridColor": ""
                                   }],
                               "allLabels": [{
                                   "text": component.get('v.xaxistitle'),
                                   "x": "!15",
                                   "y": "!15",
                                   "width": "100%",
                                   "size": 12,
                                   "bold": true,
                                   "align": "right"
                               }, {
                                   "text": component.get('v.yaxistitle'),
                                   //"rotation": 270,
                                   "x": "55",
                                   "y": "22",
                                   "width": "100%",
                                   "size": 12,
                                   "bold": true,
                                   "align": "right"
                               }],
                               
                               "dataProvider": component.get('v.data'),
                               "export": {
                                   "enabled": true,
                                   "menu": []
                               }
                           }
                          );
    },
    areaChart: function (component, helper) { // Define Area chart
        var text = "[[x]]:[[value]]";
        var bullettext = "";
        var bulletballoonttext = "";
        var bulletlabeltext = component.get('v.bullettext');
        var bulletballoonlabeltext = component.get('v.bulletballoonttext');
        if(bulletlabeltext == "x"){
            bullettext = "[[x]]";
        }
        else if(bulletlabeltext == "y"){
            bullettext = "[[value]]";
        }
            else if(bulletlabeltext == "x:y"){
                bullettext = "[[x]]:[[value]]";
            }
                else if(bulletlabeltext == "x,y"){
                    bullettext = "[[x]],[[value]]";
                }
                    else{
                        bullettext = "";  
                    }
        
        if(bulletballoonlabeltext == "x"){
            bulletballoonttext = "[[x]]";
        }
        else if(bulletballoonlabeltext == "y"){
            bulletballoonttext = "[[value]]";
        }
            else if(bulletballoonlabeltext == "x:y"){
                bulletballoonttext = "[[x]]:[[value]]";
            }
                else if(bulletballoonlabeltext == "x,y"){
                    bulletballoonttext = "[[x]],[[value]]";
                }
                    else{
                        bulletballoonttext = "";  
                    }
        AmCharts.makeChart(component.find('chartContainer').getElement(),
                           {
                               "type": "serial",
                               "theme": "light",
                               "categoryField": "x",
                               "graphs": [
                                   {
                                       "balloonText": bulletballoonttext,
                                       "labelText": bullettext,
                                       "fillAlphas": 0.7,
                                       "fillColors": ["#e6f6fc"],
                                       "labelPosition": component.get('v.bullettextposition'),
                                       "bullet": component.get('v.bullettype'),
                                       "lineColor": component.get('v.linecolor'),
                                       "valueField": "y"
                                   }
                               ],
                               "titles": [
                                   {
                                       "text": component.get('v.charttitle')
                                   }
                               ],
                               "categoryAxis": {
                                   "gridColor": ""
                               },
                               "valueAxes": [
                                   {
                                       "gridColor": ""
                                   }],
                               "allLabels": [{
                                   "text": component.get('v.xaxistitle'),
                                   "x": "!15",
                                   "y": "!15",
                                   "width": "100%",
                                   "size": 12,
                                   "bold": true,
                                   "align": "right"
                               }, {
                                   "text": component.get('v.yaxistitle'),
                                   "x": "55",
                                   "y": "22",
                                   "width": "100%",
                                   "size": 12,
                                   "bold": true,
                                   "align": "right"
                               }],
                               
                               "dataProvider": component.get('v.data'),
                               "export": {
                                   "enabled": true,
                                   "menu": []
                               }
                           }
                          );
    },
    pieDonutChart: function (component, helper) { // Define Pie and Donut chart
        var chartType = component.get('v.type');
        var innerRadius = "40%";
        if(chartType == "pie"){
            innerRadius = "0%";
        }        
        AmCharts.makeChart(component.find('chartContainer').getElement(),
                           {
                               "type": "pie",
                               "theme": "light",
                               "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                               "innerRadius": innerRadius,
                               "startDuration": 0,
                               "titleField": "x",
                               "valueField": "y",
                               "allLabels": [],
                               "balloon": {},
                               "legend": {
                                   "enabled": true,
                                   "align": "center",
                                   "markerType": "circle"
                               },
                               "titles": [],
                               "dataProvider": component.get('v.data'),
                               "export": {
                                   "enabled": true,
                                   "menu": []
                               }
                           }
                          );
    },
    columnBarChart: function (component, helper) { // Define Column and Bar chart
        var text = "[[x]]:[[value]]";
        var bullettext = "";
        var bulletballoonttext = "";
        var bulletlabeltext = component.get('v.bullettext');
        var bulletballoonlabeltext = component.get('v.bulletballoonttext');
        if(bulletlabeltext == "x"){
            bullettext = "[[x]]";
        }
        else if(bulletlabeltext == "y"){
            bullettext = "[[value]]";
        }
            else if(bulletlabeltext == "x:y"){
                bullettext = "[[x]]:[[value]]";
            }
                else if(bulletlabeltext == "x,y"){
                    bullettext = "[[x]],[[value]]";
                }
                    else{
                        bullettext = "";  
                    }
        
        if(bulletballoonlabeltext == "x"){
            bulletballoonttext = "[[x]]";
        }
        else if(bulletballoonlabeltext == "y"){
            bulletballoonttext = "[[value]]";
        }
            else if(bulletballoonlabeltext == "x:y"){
                bulletballoonttext = "[[x]]:[[value]]";
            }
                else if(bulletballoonlabeltext == "x,y"){
                    bulletballoonttext = "[[x]],[[value]]";
                }
                    else{
                        bulletballoonttext = "";  
                    }
        var chartType = component.get('v.type');
        var rotate = true
        if(chartType == "column"){
            rotate = false;
        }
        AmCharts.makeChart(component.find('chartContainer').getElement(),
                           {
                               "type": "serial",
                               "theme": "light",
                               "categoryField": "x",
                               "rotate": rotate,
                               "graphs": [
                                   {
                                       "balloonText": bulletballoonttext,
                                       "labelText": bullettext,
                                       "fillAlphas": 1,
                                       "labelPosition": component.get('v.bullettextposition'),
                                       "lineColor": component.get('v.linecolor'),
                                       "type" : "column",
                                       "valueField": "y"
                                   }
                               ],
                               "titles": [
                                   {
                                       "text": component.get('v.charttitle')
                                   }
                               ],
                               "categoryAxis": {
                                   "gridColor": ""
                               },
                               "valueAxes": [
                                   {
                                       "gridColor": ""
                                   }],
                               "allLabels": [{
                                   "text": component.get('v.xaxistitle'),
                                   "x": "!15",
                                   "y": "!15",
                                   "width": "100%",
                                   "size": 12,
                                   "bold": true,
                                   "align": "right"
                               }, {
                                   "text": component.get('v.yaxistitle'),
                                   //"rotation": 270,
                                   "x": "55",
                                   "y": "22",
                                   "width": "100%",
                                   "size": 12,
                                   "bold": true,
                                   "align": "right"
                               }],
                               
                               "dataProvider": component.get('v.data'),
                               "export": {
                                   "enabled": true,
                                   "menu": []
                               }
                           }
                          );
    },
    stackedColumnBarChart: function (component, helper) { // Define Stacked Column and Stacked Bar chart
        var text = "[[x]]:[[value]]";
        var bullettext = "";
        var bulletballoonttext = "";
        var bullettext1 = "";
        var bulletballoonttext1 = "";
        var bulletlabeltext = component.get('v.bullettext');
        var bulletballoonlabeltext = component.get('v.bulletballoonttext');
        if(bulletlabeltext == "x"){
            bullettext1 = bullettext = "[[x]]";
        }
        else if(bulletlabeltext == "y"){
            bullettext1 = bullettext = "[[value]]";
        }
            else if(bulletlabeltext == "x:y"){
                bullettext1 = bullettext = "[[x]]:[[value]]";
            }
                else if(bulletlabeltext == "x,y"){
                    bullettext1 = bullettext = "[[x]],[[value]]";
                }
                    else{
                        bullettext1 = bullettext = "";  
                    }
        
        if(bulletballoonlabeltext == "x"){
            bulletballoonttext1 = bulletballoonttext = "[[x]]";
        }
        else if(bulletballoonlabeltext == "y"){
            bulletballoonttext1 = bulletballoonttext = "[[value]]";
        }
            else if(bulletballoonlabeltext == "x:y"){
                bulletballoonttext1 = bulletballoonttext = "[[x]]:[[value]]";
            }
                else if(bulletballoonlabeltext == "x,y"){
                    bulletballoonttext1 = bulletballoonttext = "[[x]],[[value]]";
                }
                    else{
                        bulletballoonttext1 = bulletballoonttext = "";  
                    }
        var chartType = component.get('v.type');
        var rotate = true
        if(chartType == "stackedcolumn"){
            rotate = false;
        }
        AmCharts.makeChart(component.find('chartContainer').getElement(),
                           {
                               "type": "serial",
                               "theme": "light",
                               "categoryField": "x",
                               "rotate": rotate,
                               "graphs": [
                                   {
                                       "balloonText": bulletballoonttext,
                                       "labelText": bullettext,
                                       "fillAlphas": 1,
                                       "labelPosition": component.get('v.bullettextposition'),
                                       "lineColor": component.get('v.linecolor'),
                                       "type" : "column",
                                       "valueField": "y"
                                   },
                                   {
                                       "balloonText": bulletballoonttext1,
                                       "labelText": bullettext1,
                                       "fillAlphas": 1,
                                       "labelPosition": component.get('v.bullettextposition'),
                                       "lineColor": component.get('v.linesecondcolor'),
                                       "type" : "column",
                                       "valueField": "y1"
                                   }
                               ],
                               "titles": [
                                   {
                                       "text": component.get('v.charttitle')
                                   }
                               ],
                               "categoryAxis": {
                                   "gridColor": "",
                                   "stackType": "regular"
                               },
                               "valueAxes": [
                                   {
                                       "gridColor": "",
                                       "stackType": "regular"
                                   }],
                               "allLabels": [{
                                   "text": component.get('v.xaxistitle'),
                                   "x": "!15",
                                   "y": "!15",
                                   "width": "100%",
                                   "size": 12,
                                   "bold": true,
                                   "align": "right"
                               }, {
                                   "text": component.get('v.yaxistitle'),
                                   //"rotation": 270,
                                   "x": "55",
                                   "y": "22",
                                   "width": "100%",
                                   "size": 12,
                                   "bold": true,
                                   "align": "right"
                               }],
                               
                               "dataProvider": component.get('v.data'),
                               "export": {
                                   "enabled": true,
                                   "menu": []
                               }
                           }
                          );
    },
    bubbleChart: function (component, helper) { // Define Bubble chart
        var text = "[[x]]:[[value]]";
        var bullettext = "";
        var bulletballoonttext = "";
        var bulletlabeltext = component.get('v.bullettext');
        var bulletballoonlabeltext = component.get('v.bulletballoonttext');
        if(bulletlabeltext == "x"){
            bullettext = "[[x]]";
        }
        else if(bulletlabeltext == "y"){
            bullettext = "[[value]]";
        }
            else if(bulletlabeltext == "x:y"){
                bullettext = "[[x]]:[[value]]";
            }
                else if(bulletlabeltext == "x,y"){
                    bullettext = "[[x]],[[value]]";
                }
                    else{
                        bullettext = "";  
                    }
        
        if(bulletballoonlabeltext == "x"){
            bulletballoonttext = "[[x]]";
        }
        else if(bulletballoonlabeltext == "y"){
            bulletballoonttext = "[[value]]";
        }
            else if(bulletballoonlabeltext == "x:y"){
                bulletballoonttext = "[[x]]:[[value]]";
            }
                else if(bulletballoonlabeltext == "x,y"){
                    bulletballoonttext = "[[x]],[[value]]";
                }
                    else{
                        bulletballoonttext = "";  
                    }
        var data = component.get('v.data');
        
        AmCharts.makeChart(component.find('chartContainer').getElement(),
                           {
                               "type": "xy",
                               "theme": "light",
                               "trendLines": [],
                               "graphs": [
                                   {
                                       "balloonText": bulletballoonttext,
                                       "labelText": bullettext,
                                       "bullet": component.get('v.bubbletype'),
                                       "lineAlpha": component.get('v.bubbleline'),
                                       "lineColor": component.get('v.bubbleColor'),
                                       "valueField": "value",
                                       "xField": "x",
                                       "yField": "y"
                                   }
                               ],
                               "categoryAxis": {
                                   "gridColor": ""
                               },
                               "valueAxes": [
                                   {
                                       "gridColor": ""
                                   }],
                               "guides": [],
                               "valueAxes": [
                                   {
                                       "axisAlpha": 0
                                   }
                               ],
                               "allLabels": [],
                               "balloon": {},
                               "titles": [],
                               "dataProvider": component.get('v.data'),
                               "export": {
                                   "enabled": true,
                                   "menu": []
                               }
                           }
                          );
    },
    gaugeChart: function (component, helper) { // Define Gauge chart
        
        AmCharts.makeChart(component.find('chartContainer').getElement(),
                           {
                               "type": "gauge",
                               "theme": "light",
                               "startDuration": 0,
                               "arrows": [
                                   {
                                       "value": component.get('v.gaugeValue')
                                   }
                               ],
                               "axes": [
                                   {
                                       "bottomText": component.get('v.gaugebottomText'),
                                       "endValue": component.get('v.gaugeEndValue'),
                                       "valueInterval": component.get('v.gaugeValueInterval'),
                                       "bands": component.get('v.data')
                                   }
                               ],
                               "titles": [
                                   {
                                       "size": 15,
                                       "text": component.get('v.gaugeChartText')
                                   }
                               ],
                               "export": {
                                   "enabled": true,
                                   "menu": []
                               }
                           }
                          );
    },
    ganttChart: function (component, helper) { // Define Gantt chart
        
        AmCharts.makeChart( component.find('chartContainer').getElement(), {
            "type": "gantt",
            "theme": "light",
            "marginRight": component.get('v.ganttMarginRight'),
            "period": "hh",
            "dataDateFormat":"YYYY-MM-DD",
            "balloonDateFormat": "JJ:NN",
            "columnWidth": component.get('v.ganttColumnWidth'),
            "valueAxis": {
                "type": "date"
            },
            "brightnessStep": component.get('v.ganttBrightnessStep'),
            "graph": {
                "fillAlphas": component.get('v.ganttFillAlphas'),
                "lineAlpha": component.get('v.ganttLineAlpha'),
                "lineColor": component.get('v.ganttLineColor'),
                "balloonText": "<b>[[task]]</b>: [[open]] [[value]]"
            },
            "rotate": component.get('v.ganttRotate'),
            "categoryField": "y",
            "segmentsField": "x",
            "colorField": "color",
            "startDate": component.get('v.ganttStartDate'),
            "startField": "start",
            "endField": "end",
            "durationField": "duration",
            "dataProvider": component.get('v.data'),
            "valueScrollbar": {
                "autoGridCount":component.get('v.ganttAutoGridCount')
            },
            "chartCursor": {
                "cursorColor":component.get('v.ganttCursorColor'),
                "valueBalloonsEnabled": component.get('v.ganttValueBalloonsEnabled'),
                "cursorAlpha": component.get('v.ganttCursorAlpha'),
                "valueLineAlpha":component.get('v.ganttValueLineAlpha'),
                "valueLineBalloonEnabled": component.get('v.ganttValueLineBalloonEnabled'),
                "valueLineEnabled": component.get('v.ganttValueLineEnabled'),
                "zoomable":component.get('v.ganttZoomable'),
                "valueZoomable":component.get('v.ganttValueZoomable')
            },
            "export": {
                "enabled": true,
                "menu": []
            }
        } );
        
    },
    lineAreaHybridChart: function (component, helper) { // Define multiple Line Area Hybrid chart
        
        var chartBody='{"valueAxes":[{"title":"'+component.get("v.yaxistitle")+'","id":"ValueAxis-1"}],"type":"serial", "export": {"enabled": true,"menu": []},"trendLines":[],"titles":[{"text":"'+component.get("v.charttitle")+'","size":15,'+
            '"id":"Title-1"}],"legend":{"useGraphSettings":true,"enabled":true},"guides":[],"categoryField":"xAxis","categoryAxis":{'+  
            '"gridPosition":"start"},"balloon":{},"allLabels":[]';
        
        var listOfChartColors = component.get("v.linecolor").split(',');
        
        var listOfChartTypes = component.get("v.type").split(',');
        var listOfGraphTitles = component.get("v.graphtitles").split(',');
        
        var listOfChartBullets = component.get("v.typesofbullets").split(',');
        
        
        
        var graphs = '"graphs":[';
        
        var checkMultipleGraph = false;
        
        for(var i=0; i<listOfChartColors.length; i++){
            if(listOfChartTypes[i] == 'Line'){
                if(checkMultipleGraph == true){
                    graphs = graphs+',';
                }
                graphs = graphs+'{"valueField":"yAxis-'+(i+1)+'","title":"'+listOfGraphTitles[i]+'","lineColor":"'+listOfChartColors[i]+'","id":"Ampgraph-'+(i+1)+'","fillAlphas":0.0,"bullet":"'+listOfChartBullets[i]+'"}';
                checkMultipleGraph = true;
            }
            else if(listOfChartTypes[i] == 'Area'){
                if(checkMultipleGraph == true){
                    graphs = graphs+',';
                }
                graphs = graphs+'{"valueField":"yAxis-'+(i+1)+'","title":"'+listOfGraphTitles[i]+'","lineColor":"'+listOfChartColors[i]+'","id":"Ampgraph-'+(i+1)+'","fillAlphas":0.7,"bullet":"'+listOfChartBullets[i]+'"}';
                checkMultipleGraph = true;
            }
        }
        
        graphs = graphs+'],'
        var chartData = JSON.stringify(component.get('v.data'));
        chartBody = chartBody+','+graphs+'"dataProvider" :'+chartData+'}';
        AmCharts.makeChart(component.find('chartContainer').getElement(),JSON.parse(chartBody));
    }
    
    
})