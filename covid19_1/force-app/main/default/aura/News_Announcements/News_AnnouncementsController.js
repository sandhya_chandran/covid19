({
    //Component Name: News_Announcements display name "Recent News"
    //Author : Raj Rao, Principal Solution Engineer, Salesforce.com
    //Date Released : March 25, 2016
    doInit : function(component, event, helper) {
        helper.loadNewsChannelNames(component);
        helper.loadnumDocsByChannel(component);//get number of records per News Channel
        //if more link has been clicked check for channel name in the URL
        var newsURL = window.location.href;
        var startChannelName = newsURL.indexOf('channel/')+8;
        var endChannelName;
        //parse the remainder of the URL if it has query string parameters  
        if(newsURL.substring(startChannelName).indexOf('%3F') !=-1){//look for '?'
            endChannelName = newsURL.substring(startChannelName).indexOf('%3F')+startChannelName;
        }else if(newsURL.substring(startChannelName).indexOf('?') !=-1){
            endChannelName = newsURL.substring(startChannelName).indexOf('?')+startChannelName;
        }else{
            endChannelName = newsURL.length;
        }
        //var endChannelName = newsURL.length;   
		if (newsURL.indexOf('channel/') != -1) {
            var selectedChannel = decodeURI(newsURL.substring(startChannelName,endChannelName));
            component.set("v.selectedNewsChannel",selectedChannel);
            helper.loadNewsChannelRecordsByChannel(component, event);
        }else{
            //get news records by first news channel name
            helper.loadFirstNewsChannelRecords(component, event);
        }
    },   
    displayNewsChannelRecords: function(component, event, helper){
        component.set("v.selectedNewsChannel",event.target.value);
        component.set("v.encodedSelectedNewsChannel",encodeURIComponent(event.target.value));
        helper.loadNewsChannelRecordsByChannel(component); // get news records by selected news channel name
    },
 	showHideMenu : function(component, event) {
    	var cmpTarget = component.find('newsMenu');
       	var menuState = component.get('v.menuState');
        if(menuState == "Collapsed"){
            $A.util.addClass(cmpTarget, 'slds-is-open');
            component.set('v.menuState','Expanded');
        }else{
            $A.util.removeClass(cmpTarget, 'slds-is-open');
			component.set('v.menuState','Collapsed');
        } 
    },
    navigateToURL: function(component, event) {
        var address = "/recentnews";
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": address 
        });
        urlEvent.fire();
    },
    //working function to toggle LDS Tabs
    toggleTabs : function(component, event, helper){
    	var newsChannels = component.get("v.newsChannelNames");
        //var tab1 = component.find('Item1');
        var tab1content = component.find('tab-default-1__content');
        var tab2 = component.find('Item2');
        var tab2content = component.find('tab-default-2__content');
        //var clickedTab = event.target.id;
        //var clickedTabValue = event.target.value;
        var getclickedTab = event.target.parentNode.id;

        var cmpTarget = component.get("v.selectedNewsChannel");
        var cmpTarget1 = component.find(cmpTarget);
		//set clicked tab to active
        //working code below 
        $A.util.removeClass(cmpTarget1, 'slds-active');
        //$A.util.removeClass(tab1, 'slds-active');
        $A.util.removeClass(tab1content, 'slds-show');
        $A.util.addClass(tab1content, 'slds-hide');
        $A.util.addClass(tab2, 'slds-active');
        $A.util.removeClass(tab2content, 'slds-hide');
        $A.util.addClass(tab2content, 'slds-show');
    },
    /** formController.js **/
    waiting : function(component, event, helper) {
    	component.set("v.wait", "updating...");
	},
	doneWaiting : function(component, event, helper) {
    	component.set("v.wait", "");
	},
})