@RestResource (urlMapping='/GetFile/*')
global class GetFile {
    
    
    @HttpGet
    global static ContentVersion getFile() {
        RestRequest req = RestContext.request;
        String fileId = req.requestURI.substring(
          req.requestURI.lastIndexOf('/')+1);
        return 
            [SELECT Id, FileType, FileExtension,
             ContentSize, VersionData, 
             Title, ContentDocumentId, 
             IsLatest FROM ContentVersion
             WHERE id =: fileId
             AND IsLatest = true];
    }
    
    //https://csa-covid19.my.salesforce.com/services/apexrest/GetFile/0685w00000Fu4zaAAB
}