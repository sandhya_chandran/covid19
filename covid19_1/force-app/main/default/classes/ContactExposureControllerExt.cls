public with sharing class ContactExposureControllerExt {
    private final Contact con;
    public ContactExposureControllerExt(
        ApexPages.StandardController stdController
    ) {
        this.con = (Contact) stdController.getRecord();
    }
    
    public String getContactExposureJSON() {
        ContactExposureTreeNode treeNode = new ContactExposureTreeNode();
        List<ContactExposureTreeNode.ExposedIndividual> exposedIndividualList = new List<ContactExposureTreeNode.ExposedIndividual>();
        
        Contact conQueried = [SELECT id, name FROM contact WHERE id = :con.Id];
        treeNode.Id = conQueried.Id;
        treeNode.Name = conQueried.Name;
        treeNode.value = 1;
        
        // Query the first level leads
        List<Lead> exposedIndividuals = [SELECT id, Name, Contact__c, Match_Key__c
                                         FROM Lead
                                         WHERE Contact__c =: con.Id];
        
        Map<String, Id> matchKeyLeadIdMap = new Map<String,Id>();
        //Set<String> matchKeys = new Set<String>();
        
        
        for(Lead le : exposedIndividuals){
            //matchKeys.add(le.match_key__c);
            matchKeyLeadIdMap.put(le.match_key__c, le.id);
        }
        
        Map<Id, String> contactIdMatchKeyMap = new Map<Id,String>();
        
        List<Contact> cons = [select id, match_key__c from Contact where match_key__c in : matchKeyLeadIdMap.keySet()];
        
        System.debug('==== second level contacts===' + cons);
        
        for(Contact c : cons ){
            contactIdMatchKeyMap.put(c.Id, c.match_key__c);
        }
        
        System.debug('==== second level contactIdMatchKeyMap===' + contactIdMatchKeyMap);
        
        
        Map<String, List<ContactExposureTreeNode.ExposedIndividual>> matchKeyLeadsMap = new Map<String, List<ContactExposureTreeNode.ExposedIndividual>>();
 
        
        //Query the second level leads
        List<Lead> exposedLevel2Individuals = [SELECT id, Name, Contact__c  FROM Lead  WHERE Contact__c  in : cons];
        
        List<ContactExposureTreeNode.ExposedIndividual> tempEIList;
        ContactExposureTreeNode.ExposedIndividual tempEI;
        
        for(Lead sl : exposedLevel2Individuals){
            if(contactIdMatchKeyMap.containsKey(sl.Contact__c)){
                tempEI = new ContactExposureTreeNode.ExposedIndividual();
                tempEI.id = sl.id;
                tempEI.name = sl.name;
                tempEI.value = 1;
                
                if(matchKeyLeadsMap.containsKey(contactIdMatchKeyMap.get(sl.Contact__c))){
                    //find key,then update the current EIList
                    matchKeyLeadsMap.get(contactIdMatchKeyMap.get(sl.Contact__c)).add(tempEI);
                }else{
                    //cannot find key, then create a new EIList put it in the map
                    tempEIList = new List<ContactExposureTreeNode.ExposedIndividual>();
                    tempEIList.add(tempEI);
                    matchKeyLeadsMap.put(contactIdMatchKeyMap.get(sl.Contact__c), tempEIList);                    
                }         
            }else{
                
            }
        }
        
        
        List<ContactExposureTreeNode.ExposedIndividual> exposedInvList = new List<ContactExposureTreeNode.ExposedIndividual>();
        
        
        ContactExposureTreeNode.ExposedIndividual ev;
        for(Lead l : exposedIndividuals){
            ev = new ContactExposureTreeNode.ExposedIndividual();
            ev.id = l.Id;
            ev.Name = l.Name;
            ev.value = 1;
            exposedInvList.add(ev);
            if(matchKeyLeadsMap.containsKey(l.match_key__c)){
                ev.children = matchKeyLeadsMap.get(l.match_key__c);
            }else{
                ev.children = new List<ContactExposureTreeNode.ExposedIndividual>();
            }            
        }
        treeNode.children = exposedInvList;
        String jsonStr = ContactExposureTreeNode.convertToJSON(treeNode);
        
        system.debug('-> jsonStr=' + jsonStr);
        return jsonStr;
    }
}