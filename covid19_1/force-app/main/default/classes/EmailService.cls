/**
 * @File Name          : EmailService.cls
 * @Description        :
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 6/22/2020, 11:32:29 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    6/22/2020   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
 **/
public inherited sharing class EmailService {
  public EmailService() {
  }

  public static void sendHTMLMail(
    String[] addresses,
    String[] subjects,
    String[] messages
  ) {
    sendMail(addresses, subjects, messages, true);
  }

  @future
  public static void sendMail(
    String[] addresses,
    String[] subjects,
    String[] messages,
    Boolean isHTMLMail
  ) {
    system.debug('====Email Service sendMail=====');
    system.debug('====Email Service addresses=====' + addresses);
    system.debug('====Email Service subjects=====' + subjects);
    system.debug('====Email Service messages=====' + messages);
    Messaging.SingleEmailMessage[] emails = new List<Messaging.SingleEmailMessage>{};
    Integer totalMails = addresses.size();
    for (Integer i = 0; i < totalMails; i++) {
      Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
      email.setSubject(subjects[i]);
      email.setToAddresses(new List<String>{ addresses[i] });
      if (isHTMLMail) {
        email.setHtmlBody(messages[i]);
      } else {
        email.setPlainTextBody(messages[i]);
      }
      email.setReplyTo('noreply@test.com');
      emails.add(email);
    }
    Messaging.sendEmail(emails);
  }

  public class EmailSerivceException extends Exception {
  }
}