public inherited sharing class BookingSlotContactTriggerHandler extends TriggerHandler {
  List<Booking_Slot_Contact__c> newBSCList;

  List<Booking_Slot_Contact__c> newList;
  List<Booking_Slot_Contact__c> oldList;

  public BookingSlotContactTriggerHandler() {
    newList = Trigger.new;
    oldList = Trigger.old;
  }
  public override void afterUpdate() {
    List<Booking_Slot_Contact__c> cancellationConfirmList = new List<Booking_Slot_Contact__c>();
    List<Booking_Slot_Contact__c> newBookingConfirmList = new List<Booking_Slot_Contact__c>();
    List<Booking_Slot_Contact__c> recalcBookingSlotStatusList = new List<Booking_Slot_Contact__c>();
    for (integer i = 0; i < newList.size(); i++) {
      if (newList[i].Cancelled__c && !oldList[i].Cancelled__c) {
        System.debug('====cancel=====');
        cancellationConfirmList.add(newList[i]);
      }
      if (!newList[i].Cancelled__c && oldList[i].Cancelled__c) {
        newBookingConfirmList.add(newlist[i]);
      }
    }

    if (cancellationConfirmList.size() > 0) {
      BookingService.sendBookingCancellationToContact(cancellationConfirmList);
      recalcBookingSlotStatusList.addAll(cancellationConfirmList);
    }

    if (newBookingConfirmList.size() > 0) {
      BookingService.sendBookingConfirmationToContact(newBookingConfirmList);
      recalcBookingSlotStatusList.addAll(newBookingConfirmList);
    }
    if (recalcBookingSlotStatusList.size() > 0) {
      List<Id> slotIds = new List<id>();

      for (Booking_Slot_Contact__c bsc : recalcBookingSlotStatusList) {
        if (!slotIds.contains(bsc.Booking_Slot__c)) {
          slotIds.add(bsc.Booking_Slot__c);
        }
      }
      BookingService.recalcBookingSlotStatus(slotIds);
    }
  }

  public override void afterInsert() {
    List<Booking_Slot_Contact__c> cancellationConfirmList = new List<Booking_Slot_Contact__c>();
    List<Booking_Slot_Contact__c> newBookingConfirmList = new List<Booking_Slot_Contact__c>();
    List<Booking_Slot_Contact__c> recalcBookingSlotStatusList = new List<Booking_Slot_Contact__c>();

    for (integer i = 0; i < newList.size(); i++) {
      if (newList[i].Cancelled__c) {
        System.debug('====cancel=====');
        cancellationConfirmList.add(newList[i]);
      } else {
        newBookingConfirmList.add(newList[i]);
      }
    }
    if (cancellationConfirmList.size() > 0) {
      BookingService.sendBookingCancellationToContact(cancellationConfirmList);
      recalcBookingSlotStatusList.addAll(cancellationConfirmList);
    }

    if (newBookingConfirmList.size() > 0) {
      BookingService.sendBookingConfirmationToContact(newBookingConfirmList);
      recalcBookingSlotStatusList.addAll(newBookingConfirmList);
    }

    if (recalcBookingSlotStatusList.size() > 0) {
      List<Id> slotIds = new List<id>();

      for (Booking_Slot_Contact__c bsc : recalcBookingSlotStatusList) {
        if (!slotIds.contains(bsc.Booking_Slot__c)) {
          slotIds.add(bsc.Booking_Slot__c);
        }
      }
      BookingService.recalcBookingSlotStatus(slotIds);
    }
  }
}