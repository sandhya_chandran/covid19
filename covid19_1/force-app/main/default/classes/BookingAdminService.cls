/**
 * @File Name          : BookingAdminService.cls
 * @Description        :
 * @Author             : Zeeman@UserSettingsUnder.SFDoc
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 6/21/2020, 11:54:59 PM
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    6/18/2020   Zeeman@UserSettingsUnder.SFDoc     Initial Version
 **/
public with sharing class BookingAdminService {
  public BookingAdminService() {
  }
  static Integer DEFAULT_DAYS_TO_PREPOPULATE_BOOKING_SLOTS = 14;
  static Integer MAX_DAYS_LIMIT = 60;

  @AuraEnabled
  public static Map<String, String> generateBookingSlots(
    Id bookingTypeId,
    Date startDate,
    Date endDate,
    Boolean deleteOldSlots
  ) {
    Map<String, String> output = new Map<String, String>();
    Booking_Type__c bt;
    Date m_startDate;
    Date m_endDate;
    List<Booking_Slot__c> slots = new List<Booking_Slot__c>();
    try {
      if (deleteOldSlots) {
        deleteBookingSlots(bookingTypeId, startDate, endDate);
      }
      bt = getBookingType(bookingTypeId);

      if (startDate == null) {
        m_startDate = System.today();
      } else {
        m_startDate = startDate;
      }

      if (endDate == null) {
        Integer addDays = bt.Days_to_Prepopulate_Booking_Slots__c == null
          ? DEFAULT_DAYS_TO_PREPOPULATE_BOOKING_SLOTS
          : Integer.valueOf(bt.Days_to_Prepopulate_Booking_Slots__c);

        m_endDate = m_startDate.addDays(addDays);
      } else {
        m_endDate = endDate;
      }
      if (m_startDate > m_endDate) {
        throw new BookingAdminServiceException(
          'Start Date should be earlier than End Date'
        );
      }
      if (m_startDate.daysBetween(m_endDate) > MAX_DAYS_LIMIT) {
        m_endDate = m_startDate.addDays(MAX_DAYS_LIMIT);
      }

      System.debug('==== startDate === ' + m_startDate);
      System.debug('==== endDate === ' + m_endDate);
      Integer NumOfDaysToProcess = m_startDate.daysBetween(m_endDate);
      System.debug('==== NumOfDaysToProcess' + NumOfDaysToProcess);

      Date iterationDate = m_startDate;
      for (Integer i = 0; i <= NumOfDaysToProcess; i++) {
        slots.addAll(generateDaySlots(iterationDate, bt));
        iterationDate = iterationDate.addDays(1);
      }

      if (slots.size() > 0 && Schema.sObjectType.Booking_Slot__c.isCreateable()) { //Check object level create permission
        insert slots;
      }
      output.put('error', 'OK');
    } catch (Exception ex) {
      output.put('error', ex.getMessage());
      System.debug(ex);
      //throw new BookingAdminServiceException(ex.getMessage());
    }
    return output;
  }

  public static void deleteBookingSlots(
    Id bookingTypeId,
    Date startDate,
    Date endDate
  ) {
      if (Schema.sObjectType.Booking_Slot__c.isDeletable()) { //Check object-level permission for obj before deleting
        delete [
          SELECT id
          FROM Booking_Slot__c
          WHERE
            Booking_Type__c = :bookingTypeId
            AND Start_Time__c >= :startDate
            AND End_Time__c <= :endDate
        ];
      }
  }

  public static List<Booking_Slot__c> generateDaySlots(
    Date d,
    Booking_Type__c bt
  ) {
    List<Booking_Slot__c> slots = new List<Booking_Slot__c>();
    Integer dayOfWeekNumber = dayOfWeekNumber(d);

    if (dayOfWeekNumber == 0) {
      //Sunday
      if (bt.Sunday_Start_Time__c != null && bt.Sunday_End_Time__c != null) {
        slots.addAll(
          getSlotsByTimeRange(
            bt,
            d,
            bt.Sunday_Start_Time__c,
            bt.Sunday_End_Time__c
          )
        );
      }
    } else if (dayOfWeekNumber == 1) {
      //Monday
      if (bt.Monday_Start_Time__c != null && bt.Monday_End_Time__c != null) {
        slots.addAll(
          getSlotsByTimeRange(
            bt,
            d,
            bt.Monday_Start_Time__c,
            bt.Monday_End_Time__c
          )
        );
      }
    } else if (dayOfWeekNumber == 2) {
      //Tuesday
      if (bt.Tuesday_Start_Time__c != null && bt.Tuesday_End_Time__c != null) {
        slots.addAll(
          getSlotsByTimeRange(
            bt,
            d,
            bt.Tuesday_Start_Time__c,
            bt.Tuesday_End_Time__c
          )
        );
      }
    } else if (dayOfWeekNumber == 3) {
      //Wednesday

      if (
        bt.Wednesday_Start_Time__c != null &&
        bt.Wednesday_End_Time__c != null
      ) {
        slots.addAll(
          getSlotsByTimeRange(
            bt,
            d,
            bt.Wednesday_Start_Time__c,
            bt.Wednesday_End_Time__c
          )
        );
      }
    } else if (dayOfWeekNumber == 4) {
      //Thursday
      if (
        bt.Thursday_Start_Time__c != null &&
        bt.Thursday_End_Time__c != null
      ) {
        slots.addAll(
          getSlotsByTimeRange(
            bt,
            d,
            bt.Thursday_Start_Time__c,
            bt.Thursday_End_Time__c
          )
        );
      }
    } else if (dayOfWeekNumber == 5) {
      //Friday
      if (bt.Friday_Start_Time__c != null && bt.Friday_End_Time__c != null) {
        slots.addAll(
          getSlotsByTimeRange(
            bt,
            d,
            bt.Friday_Start_Time__c,
            bt.Friday_End_Time__c
          )
        );
      }
    } else if (dayOfWeekNumber == 6) {
      //Saturday_Start_Time__c
      if (
        bt.Saturday_Start_Time__c != null &&
        bt.Saturday_End_Time__c != null
      ) {
        slots.addAll(
          getSlotsByTimeRange(
            bt,
            d,
            bt.Saturday_Start_Time__c,
            bt.Saturday_End_Time__c
          )
        );
      }
    }
    return slots;
  }

  public static List<Booking_Slot__c> getSlotsByTimeRange(
    Booking_Type__c bt,
    Date d,
    Time startTime,
    Time endTime
  ) {
    List<Booking_Slot__c> slots = new List<Booking_Slot__c>();
    Booking_Slot__c slot;
    Time iterationTime = startTime;
    while (iterationTime < endTime) {
      slot = new Booking_Slot__c();
      slot.Booking_Type__c = bt.id;
      slot.Start_Time__c = DateTime.newInstance(d, iterationTime);
      iterationTime = iterationTime.addMinutes(
        Integer.valueOf(bt.Time_Slot_Duration__c)
      );
      slot.End_Time__c = DateTime.newInstance(d, iterationTime);
      slot.Max_Number_of_Booking__c = bt.Max_Number_of_Booking_Per_Slot__c;
      slots.add(slot);
    }

    return slots;
  }

  public static Integer dayOfWeekNumber(Date aDate) {
    return Math.mod(Date.newInstance(1900, 1, 7).daysBetween(aDate), 7);
  }

  public static Booking_Type__c getBookingType(Id bookingTypeId) {
    return [
      SELECT
        Active__c,
        Max_Number_of_Booking_Per_Slot__c,
        Time_Slot_Duration__c,
        Notification_Methods__c,
        Days_to_Prepopulate_Booking_Slots__c,
        Monday_Start_Time__c,
        Ignore_Holiday__c,
        Monday_End_Time__c,
        Tuesday_Start_Time__c,
        Tuesday_End_Time__c,
        Sunday_Start_Time__c,
        Sunday_End_Time__c,
        Wednesday_Start_Time__c,
        Wednesday_End_Time__c,
        Thursday_Start_Time__c,
        Thursday_End_Time__c,
        Friday_Start_Time__c,
        Friday_End_Time__c,
        Saturday_Start_Time__c,
        Saturday_End_Time__c
      FROM Booking_Type__c
      WHERE Id = :bookingTypeId
    ];
  }

  @AuraEnabled
  public static Map<String, Object> getBookingConfigureByTypeId(
    String bookingTypeId
  ) {
    Map<String, Object> output = new Map<String, Object>();

    return output;
  }

  public class BookingAdminServiceException extends Exception {
  }
}