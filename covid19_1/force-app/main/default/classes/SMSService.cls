public with sharing class SMSService {
  static String account = 'AC05348ee33e40445c58a1e5fc81d478e7';
  static String token = '85b1042ff935b99a8e98ce0aa0263b3b';

  static TwilioRestClient client;

  @future(callout=true)
  public static void sendSMS(String mobilePhone, String msg) {
    System.debug('==== sendSMS ===' + mobilePhone + ' ' + msg);
    if (client == null) {
      client = new TwilioRestClient(account, token);
    }

    Map<String, String> params = new Map<String, String>{
      'To' => mobilePhone,
      'From' => '+17059996988',
      'Body' => msg
    };
    TwilioSMS sms = client.getAccount().getSMSMessages().create(params);
  }

  public static void sendSMS(List<String> mobilePhones, List<String> msgs) {
    Integer totalSMS = mobilePhones.size();

    for (Integer i = 0; i < totalSMS; i++) {
      sendSMS(mobilePhones[i], msgs[i]);
    }
  }
}