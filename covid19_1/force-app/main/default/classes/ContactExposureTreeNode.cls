public with sharing class ContactExposureTreeNode {
    public static final String contactIcon = 'M12.075 10.812 c 1.358 -0.853 2.242 -2.507 2.242 -4.037 c 0 -2.181 -1.795 -4.618 -4.198 -4.618 S 5.921 4.594 5.921 6.775 c 0 1.53 0.884 3.185 2.242 4.037 c -3.222 0.865 -5.6 3.807 -5.6 7.298 c 0 0.23 0.189 0.42 0.42 0.42 h 14.273 c 0.23 0 0.42 -0.189 0.42 -0.42 C 17.676 14.619 15.297 11.677 12.075 10.812 M 6.761 6.775 c 0 -2.162 1.773 -3.778 3.358 -3.778 s 3.359 1.616 3.359 3.778 c 0 2.162 -1.774 3.778 -3.359 3.778 S 6.761 8.937 6.761 6.775 M 3.415 17.69 c 0.218 -3.51 3.142 -6.297 6.704 -6.297 c 3.562 0 6.486 2.787 6.705 6.297 H 3.415 Z';
    public static final String exposedSiteIcon = 'M18.121 9.88 l -7.832 -7.836 c -0.155 -0.158 -0.428 -0.155 -0.584 0 L 1.842 9.913 c -0.262 0.263 -0.073 0.705 0.292 0.705 h 2.069 v 7.042 c 0 0.227 0.187 0.414 0.414 0.414 h 3.725 c 0.228 0 0.414 -0.188 0.414 -0.414 v -3.313 h 2.483 v 3.313 c 0 0.227 0.187 0.414 0.413 0.414 h 3.726 c 0.229 0 0.414 -0.188 0.414 -0.414 v -7.042 h 2.068 h 0.004 C 18.331 10.617 18.389 10.146 18.121 9.88 M 14.963 17.245 h -2.896 v -3.313 c 0 -0.229 -0.186 -0.415 -0.414 -0.415 H 8.342 c -0.228 0 -0.414 0.187 -0.414 0.415 v 3.313 H 5.032 v -6.628 h 9.931 V 17.245 Z M 3.133 9.79 l 6.864 -6.868 l 6.867 6.868 H 3.133 Z';
    public static final String exposedIndividualIcon = 'M 15.573 11.624 c 0.568 -0.478 0.947 -1.219 0.947 -2.019 c 0 -1.37 -1.108 -2.569 -2.371 -2.569 s -2.371 1.2 -2.371 2.569 c 0 0.8 0.379 1.542 0.946 2.019 c -0.253 0.089 -0.496 0.2 -0.728 0.332 c -0.743 -0.898 -1.745 -1.573 -2.891 -1.911 c 0.877 -0.61 1.486 -1.666 1.486 -2.812 c 0 -1.79 -1.479 -3.359 -3.162 -3.359 S 4.269 5.443 4.269 7.233 c 0 1.146 0.608 2.202 1.486 2.812 c -2.454 0.725 -4.252 2.998 -4.252 5.685 c 0 0.218 0.178 0.396 0.395 0.396 h 16.203 c 0.218 0 0.396 -0.178 0.396 -0.396 C 18.497 13.831 17.273 12.216 15.573 11.624 M 12.568 9.605 c 0 -0.822 0.689 -1.779 1.581 -1.779 s 1.58 0.957 1.58 1.779 s -0.688 1.779 -1.58 1.779 S 12.568 10.427 12.568 9.605 M 5.06 7.233 c 0 -1.213 1.014 -2.569 2.371 -2.569 c 1.358 0 2.371 1.355 2.371 2.569 S 8.789 9.802 7.431 9.802 C 6.073 9.802 5.06 8.447 5.06 7.233 M 2.309 15.335 c 0.202 -2.649 2.423 -4.742 5.122 -4.742 s 4.921 2.093 5.122 4.742 H 2.309 Z M 13.346 15.335 c -0.067 -0.997 -0.382 -1.928 -0.882 -2.732 c 0.502 -0.271 1.075 -0.429 1.686 -0.429 c 1.828 0 3.338 1.385 3.535 3.161 H 13.346 Z';
    
    public String id;
    public String name;
    public Integer value;
    public String path = contactIcon;
    public ExposedIndividual[] children;
    
    public class ExposedIndividual {
        public String id; //child1
        public String name; //John Doe
        public Integer value; //1
        public String path = exposedIndividualIcon; //sddds
        public ExposedIndividual[] children;
    }
    

    public ContactExposureTreeNode() {
    }
    
    public static ContactExposureTreeNode parse(String json) {
        return (ContactExposureTreeNode) System.JSON.deserialize(
            json,
            ContactExposureTreeNode.class
        );
    }
    
    public static String convertToJSON(ContactExposureTreeNode setObj) {
        return System.JSON.serialize(setObj);
    }
}