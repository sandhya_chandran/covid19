/**
 * @File Name          : BookingService.cls
 * @Description        :
 * @Author             : Zeeman@UserSettingsUnder.SFDoc
 * @Group              :
 * @Last Modified By   : Zeeman
 * @Last Modified On   : 10-21-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    6/8/2020   Zeeman@UserSettingsUnder.SFDoc     Initial Version
 **/
public without sharing class BookingService {
  @AuraEnabled
  public static List<Booking_Type__c> getBookingTypes() {
    return [SELECT id, name FROM Booking_Type__c WHERE active__c = true];
  }

  @AuraEnabled
  public static void sendBookingConfirmationToContact(
    List<Booking_Slot_Contact__c> bscs
  ) {
    List<Id> contactIds = new List<Id>();
    Map<Id, Booking_Slot_Contact__c> contactBookingSlotContactMap = new Map<Id, Booking_Slot_Contact__c>();
    for (Booking_Slot_Contact__c bsc : bscs) {
      contactIds.add(bsc.Contact__c);
      contactBookingSlotContactMap.put(bsc.contact__c, bsc);
    }

    List<Contact> cons = [
      SELECT id, firstName, email, MobilePhone
      FROM contact
      WHERE id IN :contactIds
    ];

    List<String> addresses = new List<String>();
    List<String> subjects = new List<String>();
    List<String> messages = new List<String>();

    List<String> phones = new List<String>();
    List<String> smsMsgs = new List<String>();

    for (Contact c : cons) {
      phones.add(c.MobilePhone);
      addresses.add(c.email);
      subjects.add('Booking System Confirmation');
      String smsMsg =
        'Hi ' +
        c.firstname +
        ' Thank you for scheduling an appointment.  \nConfirmation Code: ' +
        contactBookingSlotContactMap.get(c.Id).Name +
        ' \nAppointment Type: ' +
        contactBookingSlotContactMap.get(c.Id).Booking_Type__c;
      // +
      // ' \nStart Time:';
      // +
      // contactBookingSlotContactMap.get(c.Id).Booking_Start__c.format() +
      // ' \nEnd Time:' +
      // contactBookingSlotContactMap.get(c.Id).Booking_End__c.format()

      smsMsgs.add(smsMsg);
      String body =
        'Hi ' +
        c.firstname +
        ', <br/>&nbsp;<br/>' +
        'Thank you for scheduling an appointment.';
      body +=
        '<br/> &nbsp; <br/><b>Appointment Information:</b><br/>&nbsp;<br/>' +
        'Type: <b>' +
        contactBookingSlotContactMap.get(c.Id).Booking_Type__c +
        '</b><br/>' +
        'Confirmation code: <b>' +
        contactBookingSlotContactMap.get(c.Id).Name +
        '</b>';
      body +=
        '<br/> Start Time: <b>' +
        contactBookingSlotContactMap.get(c.Id).Booking_Start__c.format() +
        '</b>';
      body +=
        '<br/> End Time: <b>' +
        contactBookingSlotContactMap.get(c.Id).Booking_End__c.format() +
        '</b>';

      body += '<br/> &nbsp; <br/> &nbsp; <br/> <i>CSA Team</i>';

      messages.add(body);
    }
    if (addresses.size() > 0) {
      try {
        EmailService.sendHTMLMail(addresses, subjects, messages);
      } catch (Exception ex) {
      }
    }
    if (smsMsgs.size() > 0) {
      try {
        SMSService.sendSMS(phones, smsMsgs);
      } catch (Exception ex) {
      }
    }
  }

  @AuraEnabled
  public static void sendBookingCancellationToContact(
    List<Booking_Slot_Contact__c> bscs
  ) {
    List<Id> contactIds = new List<Id>();
    Map<Id, Booking_Slot_Contact__c> contactBookingSlotContactMap = new Map<Id, Booking_Slot_Contact__c>();
    for (Booking_Slot_Contact__c bsc : bscs) {
      contactIds.add(bsc.Contact__c);
      contactBookingSlotContactMap.put(bsc.contact__c, bsc);
    }

    List<Contact> cons = [
      SELECT id, firstName, email, MobilePhone
      FROM contact
      WHERE id IN :contactIds
    ];

    List<String> addresses = new List<String>();
    List<String> subjects = new List<String>();
    List<String> messages = new List<String>();

    List<String> phones = new List<String>();
    List<String> smsMsgs = new List<String>();

    for (Contact c : cons) {
      phones.add(c.MobilePhone);

      String smsMsg =
        'Hi ' +
        c.firstname +
        ' This message is to confirm that your appointment has been cancelled.  \nConfirmation Code: ' +
        contactBookingSlotContactMap.get(c.Id).Name;
      //   +
      //   ' \nAppointment Type: ' +
      //   contactBookingSlotContactMap.get(c.Id).Booking_Type__c;
      //  +
      // '\nStart Time:' +
      // contactBookingSlotContactMap.get(c.Id).Booking_Start__c.format() +
      // '\nEnd Time:' +
      // contactBookingSlotContactMap.get(c.Id).Booking_End__c.format();

      smsMsgs.add(smsMsg);

      addresses.add(c.email);
      subjects.add('Booking System Cancellation Confirmation');
      String body =
        'Hi ' +
        c.firstname +
        ', <br/>&nbsp;<br/>' +
        'This email is to confirm that your appointment has been cancelled. <br/>&nbsp;<br/>';
      body +=
        '<b>Appointment Information:</br></b><br/>' +
        'Type: <b>' +
        contactBookingSlotContactMap.get(c.Id).Booking_Type__c +
        '</b><br/>Confirmation code: <b>' +
        contactBookingSlotContactMap.get(c.Id).Name +
        '</b>';
      body +=
        '<br/> Start Time: <b>' +
        contactBookingSlotContactMap.get(c.Id).Booking_Start__c.format() +
        '</b>';
      body +=
        '<br/> End Time: <b>' +
        contactBookingSlotContactMap.get(c.Id).Booking_End__c.format() +
        '</b>';

      body += '<br/> &nbsp; <br/> &nbsp; <br/> <i>CSA Team</i>';

      messages.add(body);
    }
    if (addresses.size() > 0) {
      try {
        EmailService.sendHTMLMail(addresses, subjects, messages);
      } catch (Exception ex) {
      }
    }

    if (smsMsgs.size() > 0) {
      try {
        SMSService.sendSMS(phones, smsMsgs);
      } catch (Exception ex) {
      }
    }
  }

  @AuraEnabled
  public static void recalcBookingSlotStatus(List<Id> bookingSlotIds) {
    System.debug(
      '====recalcBookingSlotStatus=== bookingSlotIds:' + bookingSlotIds
    );
    List<Booking_Slot__c> slots = [
      SELECT
        id,
        Max_Number_of_Booking__c,
        status__c,
        name,
        (SELECT id FROM Booking_Slot_Contacts__r WHERE cancelled__c = false)
      FROM booking_slot__c
      WHERE id IN :bookingSlotIds
    ];
    List<Booking_Slot__c> updateSlots = new List<Booking_Slot__c>();

    for (Booking_Slot__c s : slots) {
      if (s.status__c != 'Disabled') {
        if (s.Booking_Slot_Contacts__r.size() >= s.Max_Number_of_Booking__c) {
          s.status__c = 'Full';
          updateSlots.add(s);
        } else {
          s.status__c = 'Available';
          updateSlots.add(s);
        }
      }
    }
    System.debug('====recalcBookingSlotStatus=== updateSlots:' + updateSlots);
    if (updateSlots.size() > 0 && Schema.sObjectType.Booking_Slot__c.fields.status__c.isUpdateable() && Schema.sObjectType.Booking_Slot__c.isUpdateable()) {
      update updateSlots;
    }
  }

  @AuraEnabled
  public static Map<String, String> updateSlotStatus(
    String slotId,
    String status
  ) {
    System.debug(
      '====updateSlotStatus===slotId:' +
      slotId +
      ' Status: ' +
      status
    );
    Map<String, String> output = new Map<String, String>();
    try {
      List<Booking_Slot__c> bs = [
        SELECT id, status__c
        FROM Booking_Slot__c
        WHERE id = :slotId
      ];
      if (bs.size() == 1 && Schema.sObjectType.Booking_Slot__c.fields.status__c.isUpdateable() && Schema.sObjectType.Booking_Slot__c.isUpdateable()) {
        bs[0].status__c = status;
        update bs;
        output.put('error', 'OK');
      } else {
        output.put('error', 'Cannot find the slot with id: ' + slotId);
      }
    } catch (Exception ex) {
      output.put('error', ex.getMessage());
    }
    return output;
  }

  @AuraEnabled
  public static Map<String, String> updateBookingSlotContact(String input) {
    Map<String, String> output = new Map<String, String>();
    System.debug('====updateBookingSlotContact====' + input);
    List<UpdateBookingSlotContactDto> dtos = (List<UpdateBookingSlotContactDto>) JSON.deserializeStrict(
      input,
      List<UpdateBookingSlotContactDto>.class
    );
    System.debug('====dtos====' + dtos);

    if (dtos.size() > 0) {
      List<Booking_Slot_Contact__c> bscs = new List<Booking_Slot_Contact__c>();
      Booking_Slot_Contact__c bsc;
      for (UpdateBookingSlotContactDto dto : dtos) {
        bsc = new Booking_Slot_Contact__c();
        if (String.isNotBlank(dto.Processed)) {
          bsc.processed__c = Boolean.valueOf(dto.Processed);
        }
        if (String.isNotBlank(dto.Cancelled)) {
          bsc.cancelled__c = Boolean.valueOf(dto.Cancelled);
        }
        bsc.Id = dto.Id;

        bscs.add(bsc);
      }

      if (bscs.size() > 0 && Schema.sObjectType.Booking_Slot_Contact__c.isCreateable() && Schema.sObjectType.Booking_Slot_Contact__c.isUpdateable() && Schema.sObjectType.Booking_Slot_Contact__c.fields.processed__c.isCreateable() && Schema.sObjectType.Booking_Slot_Contact__c.fields.processed__c.isUpdateable() && Schema.sObjectType.Booking_Slot_Contact__c.fields.cancelled__c.isCreateable() && Schema.sObjectType.Booking_Slot_Contact__c.fields.cancelled__c.isUpdateable()){
        upsert bscs;
      }
      
      }

    output.put('error', 'OK');
    return output;
  }

  @AuraEnabled
  public static List<Booking_Slot_Contact__c> getSlotDetails(String slotId) {
    List<Booking_Slot_Contact__c> bscs = [
      SELECT
        id,
        Contact__r.Name,
        Contact__r.MobilePhone,
        Contact__r.Email,
        Cancelled__c,
        Processed__c
      FROM Booking_Slot_Contact__c
      WHERE Booking_Slot__c = :slotId
    ];

    return bscs;
  }

  @AuraEnabled
  public static Map<String, String> contactRegister(
    String firstName,
    String lastName,
    String mobilePhone,
    String email,
    String typeId
  ) {
    System.debug(
      '====contactRegister=== ' +
      firstName +
      ' ' +
      lastName +
      ' ' +
      mobilePhone +
      ' ' +
      email +
      ' ' +
      typeId
    );
    Map<String, String> output = new Map<String, String>();

    String matchKey = firstName + lastName + mobilePhone;
    List<contact> cons = [
      SELECT id, email
      FROM contact
      WHERE match_key__c = :matchKey
    ];
    Contact con = new Contact();
    if (cons.size() > 0) {
      //find the exsiting contact
      con.id = cons[0].id;
    }
    con.firstname = firstname;
    con.lastname = lastname;
    con.mobilephone = mobilephone;
    con.email = email;
    
     if(Schema.sObjectType.Contact.isCreateable() && Schema.sObjectType.Contact.isUpdateable() && Schema.sObjectType.Contact.fields.firstname.isCreateable() && Schema.sObjectType.Contact.fields.firstname.isUpdateable() && Schema.sObjectType.Contact.fields.lastname.isCreateable() && Schema.sObjectType.Contact.fields.lastname.isUpdateable() && Schema.sObjectType.Contact.fields.mobilephone.isCreateable() && Schema.sObjectType.Contact.fields.mobilephone.isUpdateable() && Schema.sObjectType.Contact.fields.email.isCreateable() && Schema.sObjectType.Contact.fields.email.isUpdateable()){
        upsert con;
     }
    
    System.debug('====con===' + con);

    output.put('contactId', con.Id);

    return output;
  }

  @AuraEnabled
  public static Map<String, String> bookingSlot(
    String slotId,
    String contactId,
    String bookingTypeId
  ) {
    System.debug(
      '=== booking slot === ' +
      slotId +
      ' contactId: ' +
      contactId +
      'bookingTypeId: ' +
      bookingTypeId
    );
    Map<String, String> output = new Map<String, String>();
    Integer currentSlotCount;
    Integer maxNumOfBooking;

    Booking_Slot__c currentSlot;
    //check if slot is full
    List<Booking_Slot__c> slots = [
      SELECT id, status__c, count__c, Max_Number_of_Booking__c
      FROM Booking_Slot__c
      WHERE id = :slotId
    ];

    if (slots.size() == 1) {
      currentSlotCount = Integer.valueOf(slots[0].count__c);
      currentSlot = slots[0];
      maxNumOfBooking = Integer.valueOf(slots[0].Max_Number_of_Booking__c);

      if (slots[0].status__c == 'Full') {
        output.put('isFull', 'true');
        output.put(
          'error',
          'The Slot is currently full, please choose other slot.'
        );
      }
    } else {
      output.put('error', 'unknown error');
      return output;
    }

    //check if the current contact booked this slot
    List<Booking_Slot_Contact__c> checkBooked = [
      SELECT id
      FROM Booking_Slot_Contact__c
      WHERE
        cancelled__c = false
        AND Contact__c = :contactId
        AND Booking_Slot__r.Booking_Type__c = :bookingTypeId
        AND Processed__c = false
        AND Booking_Start__c >= TODAY
    ];

    if (checkBooked.size() > 0) {
      output.put('error', 'You have booked this appointment type before.');
      return output;
    }

    List<Booking_Slot_Contact__c> bsc = [
      SELECT id, Match_Key__c
      FROM Booking_Slot_Contact__c
      WHERE Booking_Slot__c = :slotId AND cancelled__c = false
    ];

    if (bsc.size() >= maxNumOfBooking) {
      output.put('isFull', 'true');
      output.put('error', 'The slot you picked is full.');
    } else {
      Booking_Slot_Contact__c newBSC = new Booking_Slot_Contact__c();
      newBSC.Contact__c = contactId;
      newBSC.Booking_Slot__c = slotId;
      if(Schema.sObjectType.Booking_Slot_Contact__c.fields.Contact__c.isCreateable() && Schema.sObjectType.Booking_Slot_Contact__c.fields.Booking_Slot__c.isCreateable()){
          insert newBSC;
      }
      

      output.put('error', 'OK');
    }

    return output;
  }

  @AuraEnabled
  public static Map<String, Object> cancelBooking(String bookingId) {
    System.debug('==== cancelBooking Id: ===' + bookingId);
    Map<String, Object> output = new Map<String, Object>();

    List<Booking_Slot_Contact__c> bsc = [
      SELECT id, cancelled__c
      FROM Booking_Slot_Contact__c
      WHERE id = :bookingId
    ];

    if (bsc.size() == 1 && Schema.sObjectType.Booking_Slot_Contact__c.fields.Cancelled__c.isUpdateable() && Schema.sObjectType.Booking_Slot_Contact__c.isUpdateable()) {
      bsc[0].Cancelled__c = true;
      update bsc;
      output.put('error', 'OK');
    } else {
      output.put('error', 'Please contact system admin.');
    }

    return output;
  }

  @AuraEnabled
  public static List<SlotDto> getBookingSlots(
    String startDate,
    String endDate,
    String bookingTypeId
  ) {
    Datetime start;
    if (String.isEmpty(startDate)) {
      start = System.now();
    } else {
      start = System.now().addDays(-15);
    }
    List<Booking_Slot__c> bs = [
      SELECT
        id,
        Start_Time__c,
        End_Time__c,
        Count__c,
        Status__c,
        Max_Number_of_Booking__c,
        Booking_Type__r.Name
      FROM Booking_Slot__c
      WHERE Start_Time__c >= :start AND Booking_Type__c = :bookingTypeId
    ];

    List<SlotDto> dtos = new List<SlotDto>();
    SlotDto s;
    for (Booking_Slot__c b : bs) {
      s = new SlotDto(
        b.id,
        b.status__c,
        b.Start_Time__c.format(),
        b.End_Time__c.format(),
        b.status__c,
        String.valueOf(b.count__c),
        bookingTypeId,
        b.Booking_Type__r.Name,
        String.valueOf(b.Max_Number_of_Booking__c)
      );
      dtos.add(s);
    }
    System.debug('====getBookingSlots====');
    System.debug('===dtos===' + dtos);
    return dtos;
  }

  @AuraEnabled
  public static Map<String, String> searchContact(
    String mobilePhone,
    String email,
    String confirmCode
  ) {
    Map<String, String> output = new Map<String, String>();

    return output;
  }

  @AuraEnabled
  public static Map<String, Object> getBookingInfoByContact(
    String mobilePhone,
    String email,
    String confirmCode
  ) {
    System.debug('=== getBookingInfoByContact ===');
    System.debug('=== mobilePhone ===' + mobilePhone);
    System.debug('=== email ===' + email);
    System.debug('=== confirmCode ===' + confirmCode);

    Map<String, Object> output = new Map<String, Object>();

    String queryStr = 'SELECT Id,Name,Booking_Start__c,Booking_End__c,Booking_Type__c FROM Booking_Slot_Contact__c WHERE Name=: confirmCode';
    if (String.isNotBlank(email)) {
      queryStr += ' AND Contact_Email__c = :email';
    }

    if (String.isNotBlank(mobilePhone)) {
      queryStr += ' AND Contact_Phone__c =:mobilePhone ';
    }

    List<sObject> sobjList = Database.query(queryStr);

    if (sobjList.size() == 1) {
      output.put('error', 'OK');
      BookingInfoDto dto = new BookingInfoDto();
      dto.Id = sobjList[0].Id;
      dto.StartTime = ((Booking_Slot_Contact__c) sobjList[0])
        .Booking_Start__c.format();
      dto.EndTime = ((Booking_Slot_Contact__c) sobjList[0])
        .Booking_End__c.format();
      dto.BookingType = ((Booking_Slot_Contact__c) sobjList[0]).Booking_Type__c;
      output.put('infos', dto);
    } else {
      output.put(
        'error',
        'Could not find any appointment in system. Please check your typed information.'
      );
    }
    return output;
  }

  @AuraEnabled
  public static Map<String, Object> getConfig(String bookingTypeId) {
    Map<String, Object> output = new Map<String, Object>();
    List<Booking_Type__c> bookingtypes = [
      SELECT Id, Notification_Methods__c
      FROM Booking_Type__c
      WHERE Id = :bookingTypeId
    ];
    if (bookingtypes.size() == 1) {
      output.put('error', 'OK');
      output.put('error', bookingtypes[0]);
    } else {
      output.put(
        'error',
        'Error: BookingService.getConfig throws an exception'
      );
    }
    return output;
  }

  /* ================ DTOs   ==========================*/

  public class SlotDto {
    public SlotDto() {
    }
    public SlotDto(
      String id,
      String title,
      String startStr,
      String endStr,
      String status,
      String count,
      String typeId,
      String typeName,
      String maxBookingNumber
    ) {
      if (status == 'Full') {
        this.borderColor = 'red';
        this.backgroundColor = 'WhiteSmoke';
      } else if (status == 'Available') {
        this.borderColor = 'green';
        this.backgroundColor = '#87CEFA';
      } else {
        this.borderColor = 'black';
        this.backgroundColor = 'WhiteSmoke';
      }
      this.id = id;
      this.title = title;
      this.startStr = startStr;
      this.endStr = endStr;
      this.textColor = 'black';
      this.status = status;
      this.count = count;
      this.typeId = typeId;
      this.typeName = typeName;
      this.maxBookingNumber = maxBookingNumber;
    }

    @AuraEnabled
    public String id { get; set; }
    @AuraEnabled
    public String title { get; set; }
    @AuraEnabled
    public String startStr { get; set; }
    @AuraEnabled
    public String endStr { get; set; }
    @AuraEnabled
    public String borderColor { get; set; }
    @AuraEnabled
    public String backgroundColor { get; set; }
    @AuraEnabled
    public String textColor { get; set; }
    @AuraEnabled
    public String status { get; set; }
    @AuraEnabled
    public String count { get; set; }
    @AuraEnabled
    public String typeId { get; set; }
    @AuraEnabled
    public String typeName { get; set; }
    @AuraEnabled
    public String maxBookingNumber { get; set; }
  }

  class BookingInfoDto {
    @AuraEnabled
    public String Id { get; set; }
    @AuraEnabled
    public String StartTime { get; set; }
    @AuraEnabled
    public String EndTime { get; set; }
    @AuraEnabled
    public String BookingType { get; set; }
  }

  class UpdateBookingSlotContactDto {
    String Id { get; set; }
    String Processed { get; set; }
    String Cancelled { get; set; }
  }
  class UpdateBookingSlotContactDtos {
    List<UpdateBookingSlotContactDto> dots { get; set; }
  }
}