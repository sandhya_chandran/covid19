import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import { encodeDefaultFieldValues } from 'lightning/pageReferenceUtils';
import leaflet from '@salesforce/resourceUrl/leaflet17';


export default class GISBaseMapLayer extends NavigationMixin(LightningElement){
    @api markers;
    @api center;
    @api zoom;
    @api icon;
    @api mapheight;
    @api layersettings;
    @api showservicemap;
    customicon = leaflet+'/images/custom/';
    shadowUrl=leaflet+'/images/marker-shadow.png';
    latlngArea = [];
    renderedCallback() {
        Promise.all([
            loadScript(this, leaflet + '/leaflet.js'),
            loadStyle(this, leaflet + '/leaflet.css')
        ])
            .then(() => {
                this.initializeleaflet();
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error Occurred',
                        message: error.message,
                        variant: 'error'
                    })
                );
            });
    }
    generatePopupWindow(item){
        let taburl='lightning/r/Property__c/'+item.locationId+'/view';
        let title = '<a href="/' + taburl + '" >'+item.name+'</a>';
        let popuphtml='<div class="TitleDiv"><b>'+title+'</b></div>';
        popuphtml+='<div ><table class="cpopUpTable">';
        popuphtml+='<tr><td><b>Address:</b></td><td>'+item.street+'</td></tr>';
        popuphtml+='<tr><td><b>City:</b></td><td>'+item.city+'</td></tr>';
        popuphtml+='</table></div>';
        return popuphtml;
    }
    initializeleaflet() {

        this.template.querySelector(".map-root").style.height = this.mapheight+"px";
        //document.getElementById("myBtn").style.height = "50px";
        let options;
        if (this.icon) {
            //custom marker
            let customMarker = L.icon({
                iconUrl: leaflet+'/images/custom/'+this.icon,
                shadowUrl:leaflet+'/images/marker-shadow.png',
                iconSize: [25, 41], 
                iconAnchor:[12, 41], 
                shadowSize: [41, 41], 
                shadowAnchor: [12, 41],  
                popupAnchor:  [0, -25],
            });

            options = { icon: customMarker };
     }

        let markerLayers = [];
        let latlngArea = [];

        

        var self=this;

        this.markers.forEach(marker => {
           
            let myPopup =  document.createElement("div",'customPopupContainer');
         
            myPopup.innerHTML = this.generatePopupWindow(marker);
            let btnCreateTask = L.DomUtil.create('span', 'customButton');
            let btnCreateEvent =  L.DomUtil.create('span', 'customButton');
            let buttonGroup =  L.DomUtil.create('div', 'buttonGroup');
           
            btnCreateTask.innerHTML = 'Assign Task';
            btnCreateEvent.innerHTML = 'Book a Meeting';

            buttonGroup.appendChild(btnCreateTask);
            buttonGroup.appendChild(btnCreateEvent);
            myPopup.appendChild(buttonGroup);
           
            btnCreateTask.onclick = function () {
                self.cButtonCreateTask(marker.locationId);
            };
            btnCreateEvent.onclick = function () {
                self.cButtonCreateEvent(marker.locationId);
            };


            markerLayers.push(L.marker([marker.latitude, marker.longitude], options).bindPopup(myPopup,{
                minWidth : 200}));

            latlngArea.push([marker.latitude, marker.longitude]);
        });



        var areas = L.layerGroup(markerLayers);
/*
        var baseLayer = L.tileLayer('https://server.arcgisonline.com/arcgis/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
            attribution: '&copy; ' + ' Contributors',
            maxZoom: 18,
        });
        var topography = L.tileLayer('https://server.arcgisonline.com/arcgis/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}', {
			maxZoom: 18,
			attribution: '&copy; ' + ' Contributors',
		});
        var satellite = L.tileLayer('https://server.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
			maxZoom: 18,
			attribution: '&copy; ' + ' Contributors',
        });
        
        var openStr = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			maxZoom: 18,
			attribution: '&copy; ' + ' Contributors',
		});
*/
        var viewLayers = { };

        console.log('==========='+this.layersettings);

        for (const key in this.layersettings) {
            let serverMapCopyright='';
            if(this.showservicemap){
                serverMapCopyright= '<a href="'+this.layersettings[key].Service_Site__c+'">'+this.layersettings[key].Service_Name__c+'</a>';
            }
            var tempLayer=L.tileLayer(this.layersettings[key].Layer_URL__c, {
            maxZoom: 18,
            attribution: '&copy; ' + serverMapCopyright+' Contributors',
            });
            viewLayers[this.layersettings[key].Label]=tempLayer;

        }

        
        if(Object.keys(viewLayers).length === 0){
            alert('No Available Layers');
            return;
        }
        var defaultLayerKey=Object.keys(viewLayers)[0];

        const mapRoot = this.template.querySelector(".map-root");
        var map = L.map(mapRoot, {
            center: [this.center.latitude, this.center.longitude],
            zoom: this.zoom || 16,
            layers: [viewLayers[defaultLayerKey], areas]
        });

        L.control.layers(viewLayers).addTo(map);
        map.fitBounds(L.polygon(latlngArea).getBounds());	

    }

    cButtonCreateEvent(itemId) {
        this.createRecord('Event',itemId);
      }
      
      cButtonCreateTask(itemId) {
        this.createRecord('Task',itemId);
      }
      createRecord2(objApi,parentId){

        var creaturl="/lightning/o/"+objApi+"/new?defaultFieldValues=WhatId="+parentId;
        window.open(creaturl);
    }
    openViewRecordForm(objApi,recordId){



    }
      createRecord(objApi,parentID){
        let pageRef = {
            type: "standard__objectPage",
            attributes: {
                objectApiName: objApi,
                actionName: "new"
            },
            state: {
            }
        };
        const defaultFieldValues = {
            WhatId: parentID
        };
        pageRef.state.defaultFieldValues = encodeDefaultFieldValues(defaultFieldValues);
        this[NavigationMixin.Navigate](pageRef);



    }
   
}