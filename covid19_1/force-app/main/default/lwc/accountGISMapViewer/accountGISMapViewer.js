import { api, LightningElement, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getLocations from '@salesforce/apex/AccountGISMapController.getLocations';

export default class AccountGISMapViewer extends LightningElement {
    @track showMap = false;
    @track locations;
    @track center;
    @track layersettings;
    @api mapheight;
    @api showservicemap;
    @api custommarkericon;
    initiated = false;
    getCurrentLocationOnLoad() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                this.center = {};
                // set location to center the map like below
                this.center.latitude = position.coords.latitude;
                this.center.longitude = position.coords.longitude;
            });
        } else {
            const evt = new ShowToastEvent({
                title: 'Geolocation is not supported by browser',
                message: 'Check your browser options or use another browser',
                variant: 'error',
            });
            this.dispatchEvent(evt);
        }
    }

    generatePopupWindow(item){
        let taburl='lightning/r/Property__c/'+item.locationId+'/view';
        let title = '<a href="/' + taburl + '" >'+item.name+'</a>';
        let popuphtml='<div class="TitleDiv"><b>'+title+'</b></div>';
        popuphtml+='<table>';
        popuphtml+='<tr><td><b>City:</b></td><td>'+item.city+'</td></tr>';
        popuphtml+='</table>';
        return popuphtml;
    }
    
    loadAccountLocations(center) {
        
        getLocations()
            .then(result => {
                this.locations = [];
                console.log('===333='+JSON.stringify(result.layerSettings));

                this.layersettings=result.layerSettings;

                result.locations.forEach(location => {
                    let temp = { ...location };
                    //temp.content = self.generatePopupWindow(temp);
                    this.locations.push(temp);
                    
                });
                this.showMap = true;
            })
            .catch(error => {
                console.log('error', error);
                this.error = error;
            });
    }

    renderedCallback() {
        if (!this.initiated) {
            // this.getCurrentLocationOnLoad();
            this.center = {};
            this.center.latitude = 43.598888;
            this.center.longitude = -79.629702;
            this.loadAccountLocations(this.center);
            this.initiated = true;
        }
    }
}