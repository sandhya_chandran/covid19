trigger getRegistrationId on Event_Registration__c (before insert, after update) {
    
    
    Set<Id> setEventName = new Set<Id>();
    Map<Id,public_event__c> mapPublicEvent = new Map<Id,public_event__c>();
    Set<Id> PEIds = new Set<Id>();
        
    for(Event_Registration__c er : trigger.new){
        setEventName.add(er.event_name__c);
    }
        
    for(public_event__c pe : [select id, name, maximum_capacity__c, waitlisting_capacity__c from public_event__c where Id IN: setEventName]){
        mapPublicEvent.put(pe.Id,pe);
        PEIds.add(pe.Id);
    }

    if(Trigger.isBefore){
    String EventName;
    Integer NumberOfWords;
    List<String> words;
    String eventcode='';
    
    for(Event_Registration__c er : trigger.new){
        public_event__c pe = mapPublicEvent.get(er.event_name__c);
    
        EventName = pe.name;
        system.debug('event name is' +EventName);
        EventName = EventName.toUpperCase();
        words = EventName.split('\\s+');
        NumberOfWords = words.size();
        for(integer i = 0; i < NumberOfWords; i++){
            eventcode += words[i].substring(0,1);
        }
        if(eventcode != null){
         er.event_code__c = eventcode;    
        }
     }

  }
    
    if(Trigger.isAfter){
        List<event_registration__c> updatewaitlistbookings = new List<event_registration__c>();
        list<event_registration__c> getallregisteredevents = new list<event_registration__c>();
        decimal availableseats;
        decimal availablewaitlistseats;
        decimal bookedconfirmedseats = 0;
        decimal bookedwaitlistseats = 0;
        string s;
        Map<Id,List<Event_registration__c>> mapEventRegisteration = new Map<Id,List<Event_registration__c>>();
        List<Event_registration__c> eventReg = new List<Event_registration__c>();
        
        for(Event_registration__c event : [select id, confirmed_seats__c, waitlist_seats__c,event_name__c , registration_status__c from Event_registration__c]){
            if(!mapEventRegisteration.containsKey(event.event_name__c)){
                eventReg = new List<Event_registration__c>();
                eventReg.add(event);
                mapEventRegisteration.put(event.event_name__c,eventReg);
            }else{
                eventReg = new List<Event_registration__c>();
                eventReg.add(event);
                mapEventRegisteration.put(event.event_name__c,eventReg);
            } 
        }
        
        for(Event_registration__c er:trigger.new){
            if(er.Registration_Status__c == 'Canceled'){
                public_event__c pe = mapPublicEvent.get(er.event_name__c);
                
                for(Event_registration__c ge : mapEventRegisteration.get(er.event_name__c)){
                 
                    bookedconfirmedseats = bookedconfirmedseats + ge.confirmed_seats__c;
                    //bookedwaitlistseats = bookedwaitlistseats + ge.waitlist_seats__c;
                   
                    //if(ge.Registration_Status__c == 'Waitlist'){
                    //    updatewaitlistbookings.add(ge);
                    //}
                }
                  
                availableseats = pe.maximum_capacity__c - bookedconfirmedseats;
                availablewaitlistseats = pe.waitlisting_capacity__c - bookedwaitlistseats;    
                system.debug('current waitlist bookings are - '+updatewaitlistbookings);
                system.debug('available seats '+availableseats);
                system.debug('available waitlist seats '+availablewaitlistseats);
            }
        }    
        
            
        if(availableseats!=0 && !updatewaitlistbookings.isEmpty()){
            for(event_registration__c e:updatewaitlistbookings){
                if(e.waitlist_seats__c == 0){
                    //do nothing
                }
                else if(e.waitlist_seats__c <= availableseats){
                    e.Confirmed_Seats__c = e.Confirmed_Seats__c + e.Waitlist_Seats__c;
                    e.Waitlist_Seats__c = 0;
                    e.Registration_Status__c = 'Confirmed';
                    availableseats = availableseats - e.waitlist_seats__c;
                } else if(e.waitlist_seats__c > availableseats){
                    e.Confirmed_Seats__c = e.Confirmed_Seats__c + availableseats;
                    e.Waitlist_Seats__c = e.Waitlist_Seats__c - availableseats;
                    availableseats = 0;
                }
            }  
            
       try{
          update updatewaitlistbookings;
          s = 'success';
        }
        catch (DMLexception e){
          s = e.getMessage();
        }
        system.debug('DML update result is'+s);
        }
        
    }
    
}