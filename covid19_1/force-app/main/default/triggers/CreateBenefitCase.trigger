/* -----------------------------------------------------------------------------------------------------------------------
Trigger Name:       CreateBenefitCase 
Description:        
----------------------------------------------------------------------------------------------------------------------------
Date         Version          Author               Summary of Changes 
-----------  -------  ------------------------  ------------------------------------------------------------------------------
02/10/2021     1.0            Sandhya              Removed DML and SOQL from Loop
-------------------------------------------------------------------------------------------------------------------------- 
*/

trigger CreateBenefitCase on vlocity_ps__ProgramEnrollment__c (after update) {

    List<vlocity_ps__Case__c> lstbc = new List<vlocity_ps__Case__c>();
    Set<Id> PEIds = new Set<Id>();
    Map<Id,Id> bc_count = new Map<Id,Id>();
    Set<Id> existingContacts = new Set<Id>();
    
     for(vlocity_ps__ProgramEnrollment__c pe : Trigger.new){
         PEIds.add(pe.Id);
         existingContacts.add(pe.vlocity_ps__PrimaryContactId__c);
     }
    
     // search for existing Benefit Cases
     for(vlocity_ps__Case__c bcase: [SELECT Id,Program_Enrollment__c from vlocity_ps__Case__c WHERE Program_Enrollment__c IN: PEIds]){
         if(bcase.Program_Enrollment__c != null){
              bc_count.put(bcase.Program_Enrollment__c,bcase.Id);   
          }
     }
     
     Map<ID, Contact> caseContacts = new Map<ID, Contact>([SELECT id,FirstName, LastName FROM Contact where Id IN: existingContacts]);
    
     for( vlocity_ps__ProgramEnrollment__c pe : Trigger.new)
        {
        if ((!bc_count.containsKey(pe.Id)) && (pe.vlocity_ps__Status__c == 'Approved') && (pe.vlocity_ps__Type__c == 'SNAP'))  // No Benefit Cases found, but PE is Approved & Type is SNAP
        {
            vlocity_ps__Case__c bc = new vlocity_ps__Case__c();
            if (pe.vlocity_ps__PrimaryContactId__c != null )  // make sure there is a contact
               {
               Contact con = new Contact();//[SELECT FirstName, LastName FROM Contact where Id =: pe.vlocity_ps__PrimaryContactId__c ];
               
               bc.Name = caseContacts.get(pe.vlocity_ps__PrimaryContactId__c).FirstName  + ' ' + caseContacts.get(pe.vlocity_ps__PrimaryContactId__c).LastName + ' - SNAP';
               bc.vlocity_ps__PrimaryContactId__c = pe.vlocity_ps__PrimaryContactId__c;
               }
            bc.vlocity_ps__Stage__c = 'Planning';
            bc.vlocity_ps__Type__c = 'Food Assistance';
            bc.vlocity_ps__ProgramId__c = pe.vlocity_ps__ProgramId__c;  // link to the Program
            bc.Program_Enrollment__c = pe.Id;  // link to the Program Enrollment
            lstbc.add(bc); //added bc in list
        }
    }


        try{
            if(lstbc != null){
               insert lstbc;
            } 
        } catch(system.Dmlexception e) {
            system.debug (e);
        }
}