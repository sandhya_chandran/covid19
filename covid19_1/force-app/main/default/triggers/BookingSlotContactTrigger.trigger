trigger BookingSlotContactTrigger on Booking_Slot_Contact__c(
  after insert,
  after update,
  before insert,
  before update,
  before delete,
  after undelete,
  after delete
) {
  new BookingSlotContactTriggerHandler().run();

}